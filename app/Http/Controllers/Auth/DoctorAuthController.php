<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class DoctorAuthController extends Controller
{

    public function index()
    {
        return view('auth.doctor_login');
    }
    public function username()
    {
        return 'username';
    }
    public function login(Request $request)
    {
        //dd($request->all());
        $username = $request->input($this->username());

        $field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile';
        $credentials = [
            $field => $username,
            'password' => $request->input('password'),
        ];
       // $credentials = $request->only('email', 'password');

        if (Auth::guard('doctor')->attempt($credentials)) {
            // Authentication successful, redirect to admin dashboard
            return redirect()->intended('doctor-list');
        }

        // Authentication failed, redirect back to login with error message
        return redirect()->back()->withErrors(['email' => 'Invalid credentials']);
    }
}
