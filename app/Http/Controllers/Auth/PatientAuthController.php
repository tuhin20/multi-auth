<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class PatientAuthController extends Controller
{

    public function index()
    {
        return view('auth.patient_login');
    }
    public function username()
    {
        return 'username';
    }
    public function login(Request $request)
    {
        //dd($request->all());
        $username = $request->input($this->username());

        $field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile';
        $credentials = [
            $field => $username,
            'password' => $request->input('password'),
        ];

        if (Auth::guard('patient')->attempt($credentials)) {
            // Authentication successful
            return redirect()->intended('patiant-list');
        }

        // Authentication failed, redirect back to login with error message
        return redirect()->back()->withErrors(['email' => 'Invalid credentials']);
    }
}
