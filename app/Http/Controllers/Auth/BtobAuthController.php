<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class BtobAuthController extends Controller
{

    public function index()
    {
        return view('auth.btob_login');
    }
    public function username()
    {
        return 'username';
    }
    public function login(Request $request)
    {
        //dd($request->all());
        // $credentials = $request->only('email', 'password');
        $username = $request->input($this->username());

        $field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile';
        $credentials = [
            $field => $username,
            'password' => $request->input('password'),
        ];

        if (Auth::guard('btob')->attempt($credentials)) {
            // Authentication successful, redirect to admin dashboard
            return redirect()->intended('btob-list');
        }

        // Authentication failed, redirect back to login with error message
        return redirect()->back()->withErrors(['email' => 'Invalid credentials']);
    }
}
