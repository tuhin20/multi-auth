<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/patient', [App\Http\Controllers\Auth\PatientAuthController::class, 'index']);
Route::post('/patient-login', [App\Http\Controllers\Auth\PatientAuthController::class, 'login'])->name('patient-login');
Route::get('/doctor', [App\Http\Controllers\Auth\DoctorAuthController::class, 'index']);
Route::post('/doctor-login', [App\Http\Controllers\Auth\DoctorAuthController::class, 'login'])->name('doctor-login');
Route::get('/btob', [App\Http\Controllers\Auth\BtobAuthController::class, 'index']);
Route::post('/btob-login', [App\Http\Controllers\Auth\BtobAuthController::class, 'login'])->name('btob-login');
Route::group(['middleware' => 'auth:patient'], function () {
    Route::get('/patiant-list', [App\Http\Controllers\PatientController::class, 'index']);
});

Route::group(['middleware' => 'auth:doctor'], function () {
    Route::get('/doctor-list', [App\Http\Controllers\DoctorController::class, 'index']);
});
Route::group(['middleware' => 'auth:btob'], function () {
    Route::get('/btob-list', [App\Http\Controllers\BtobController::class, 'index']);
});